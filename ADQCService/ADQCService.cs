﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using AudioDeviceUtil;

namespace ADQCService
{
    public partial class ADQCService : ServiceBase
    {
        #region Fields

        private Timer _tmrCheckDevices;
        private int _DeviceCount;
        private AudioDeviceManager _ADM;

        #endregion

        #region Constructors

        public ADQCService()
        {
            InitializeComponent();
        }

        #endregion

        #region Start/Stop

        protected override void OnStart(string[] args)
        {
            /**************************************/
            System.Diagnostics.Debugger.Launch();
            while (!Debugger.IsAttached)
            {
                System.Threading.Thread.Sleep(1000);
            }
            /**************************************/

            this._tmrCheckDevices = new Timer();
            this._tmrCheckDevices.Interval = 1000;   //5000?
            this._tmrCheckDevices.Elapsed += new ElapsedEventHandler(Timer_Elapsed);

            this._ADM = new AudioDeviceManager(true);

            this._ADM.UpdatePlaybackDeviceList();
            //this.devices = _ADM.PlaybackDevices.Where(d => d.DeviceState == AudioDeviceStateType.)
            this._DeviceCount = this._ADM.PlaybackDevices.Where(d => d.DeviceState == AudioDeviceStateType.Active).Count();

            //this._audioNotify.AudioDeviceEvent += new EventHandler<AudioDeviceNotificationEventArgs>(AudioDevice_Event);

            /* See: https://msdn.microsoft.com/en-us/library/zt39148a(v=vs.110).aspx for info on ServiceStatus interop */

            this._tmrCheckDevices.Start();
        }

        protected override void OnStop()
        {
            this._tmrCheckDevices.Stop();
            this._tmrCheckDevices.Dispose();
        }

        #endregion

        #region Methods

        public bool DeviceWasChanged()
        {
            _ADM.UpdatePlaybackDeviceList();
            if (_ADM.PlaybackDevices.Where(d => d.DeviceState == AudioDeviceStateType.Active).Count() != this._DeviceCount)
            {
                return (true);
            }
            return (false);
        }

        #endregion

        #region Event Handlers

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            this._tmrCheckDevices.Stop();

            //Check for device changes
            _ADM.UpdatePlaybackDeviceList();
            
            //DBG
            //Debug.WriteLine($"ADMCOUNT: {_ADM.PlaybackDevices.Where(d => d.DeviceState == AudioDeviceStateType.Active).Count()}");
            //Debug.WriteLine($"DCOUNT: {this._DeviceCount}");
            //DBG

            if (DeviceWasChanged())
            {
                this._DeviceCount = _ADM.PlaybackDevices.Where(d => d.DeviceState == AudioDeviceStateType.Active).Count();
            }

            this._tmrCheckDevices.Start();
        }

        #endregion
    }
}
