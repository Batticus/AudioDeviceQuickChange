﻿namespace AudioDeviceQuickChange
{
    partial class ADQCPreferencesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.chkShowAudioDevicesAsMenuItem = new System.Windows.Forms.CheckBox();
            this.btnDefaults = new System.Windows.Forms.Button();
            this.grpbxShownDevices = new System.Windows.Forms.GroupBox();
            this.chkShowActive = new System.Windows.Forms.CheckBox();
            this.chkShowDisabled = new System.Windows.Forms.CheckBox();
            this.chkShowUnplugged = new System.Windows.Forms.CheckBox();
            this.chkShowNotifyIconOnStart = new System.Windows.Forms.CheckBox();
            this.chkLaunchAtWindowsStartup = new System.Windows.Forms.CheckBox();
            this.grpbxAudioDeviceOptions = new System.Windows.Forms.GroupBox();
            this.grpbxHotkey = new System.Windows.Forms.GroupBox();
            this.chkEnableHotkey = new System.Windows.Forms.CheckBox();
            this.btnSetHotkey = new System.Windows.Forms.Button();
            this.txtHotkey = new System.Windows.Forms.TextBox();
            this.btnDefaultDevice = new System.Windows.Forms.Button();
            this.lblStartupDevice = new System.Windows.Forms.Label();
            this.btnActivateOnStartup = new System.Windows.Forms.Button();
            this.lstbxAudioDevices = new System.Windows.Forms.ListBox();
            this.btnAbout = new System.Windows.Forms.Button();
            this.grpbxShownDevices.SuspendLayout();
            this.grpbxAudioDeviceOptions.SuspendLayout();
            this.grpbxHotkey.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(429, 290);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 0;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnOK
            // 
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(348, 290);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 1;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // chkShowAudioDevicesAsMenuItem
            // 
            this.chkShowAudioDevicesAsMenuItem.AutoSize = true;
            this.chkShowAudioDevicesAsMenuItem.Location = new System.Drawing.Point(119, 12);
            this.chkShowAudioDevicesAsMenuItem.Name = "chkShowAudioDevicesAsMenuItem";
            this.chkShowAudioDevicesAsMenuItem.Size = new System.Drawing.Size(189, 17);
            this.chkShowAudioDevicesAsMenuItem.TabIndex = 2;
            this.chkShowAudioDevicesAsMenuItem.Text = "Show audio devices as MenuItem.";
            this.chkShowAudioDevicesAsMenuItem.UseVisualStyleBackColor = true;
            // 
            // btnDefaults
            // 
            this.btnDefaults.Location = new System.Drawing.Point(12, 290);
            this.btnDefaults.Name = "btnDefaults";
            this.btnDefaults.Size = new System.Drawing.Size(75, 23);
            this.btnDefaults.TabIndex = 3;
            this.btnDefaults.Text = "Defaults";
            this.btnDefaults.UseVisualStyleBackColor = true;
            this.btnDefaults.Click += new System.EventHandler(this.btnDefaults_Click);
            // 
            // grpbxShownDevices
            // 
            this.grpbxShownDevices.Controls.Add(this.chkShowActive);
            this.grpbxShownDevices.Controls.Add(this.chkShowDisabled);
            this.grpbxShownDevices.Controls.Add(this.chkShowUnplugged);
            this.grpbxShownDevices.Location = new System.Drawing.Point(12, 12);
            this.grpbxShownDevices.Name = "grpbxShownDevices";
            this.grpbxShownDevices.Size = new System.Drawing.Size(101, 88);
            this.grpbxShownDevices.TabIndex = 4;
            this.grpbxShownDevices.TabStop = false;
            this.grpbxShownDevices.Text = "Shown Devices:";
            // 
            // chkShowActive
            // 
            this.chkShowActive.AutoSize = true;
            this.chkShowActive.Location = new System.Drawing.Point(6, 19);
            this.chkShowActive.Name = "chkShowActive";
            this.chkShowActive.Size = new System.Drawing.Size(56, 17);
            this.chkShowActive.TabIndex = 2;
            this.chkShowActive.Text = "Active";
            this.chkShowActive.UseVisualStyleBackColor = true;
            this.chkShowActive.CheckedChanged += new System.EventHandler(this.ShownDevices_CheckedChanged);
            // 
            // chkShowDisabled
            // 
            this.chkShowDisabled.AutoSize = true;
            this.chkShowDisabled.Location = new System.Drawing.Point(6, 42);
            this.chkShowDisabled.Name = "chkShowDisabled";
            this.chkShowDisabled.Size = new System.Drawing.Size(67, 17);
            this.chkShowDisabled.TabIndex = 1;
            this.chkShowDisabled.Text = "Disabled";
            this.chkShowDisabled.UseVisualStyleBackColor = true;
            this.chkShowDisabled.CheckedChanged += new System.EventHandler(this.ShownDevices_CheckedChanged);
            // 
            // chkShowUnplugged
            // 
            this.chkShowUnplugged.AutoSize = true;
            this.chkShowUnplugged.Location = new System.Drawing.Point(6, 65);
            this.chkShowUnplugged.Name = "chkShowUnplugged";
            this.chkShowUnplugged.Size = new System.Drawing.Size(78, 17);
            this.chkShowUnplugged.TabIndex = 0;
            this.chkShowUnplugged.Text = "Unplugged";
            this.chkShowUnplugged.UseVisualStyleBackColor = true;
            this.chkShowUnplugged.CheckedChanged += new System.EventHandler(this.ShownDevices_CheckedChanged);
            // 
            // chkShowNotifyIconOnStart
            // 
            this.chkShowNotifyIconOnStart.AutoSize = true;
            this.chkShowNotifyIconOnStart.Location = new System.Drawing.Point(119, 35);
            this.chkShowNotifyIconOnStart.Name = "chkShowNotifyIconOnStart";
            this.chkShowNotifyIconOnStart.Size = new System.Drawing.Size(145, 17);
            this.chkShowNotifyIconOnStart.TabIndex = 5;
            this.chkShowNotifyIconOnStart.Text = "Show NotifyIcon on start.";
            this.chkShowNotifyIconOnStart.UseVisualStyleBackColor = true;
            // 
            // chkLaunchAtWindowsStartup
            // 
            this.chkLaunchAtWindowsStartup.AutoSize = true;
            this.chkLaunchAtWindowsStartup.Location = new System.Drawing.Point(119, 83);
            this.chkLaunchAtWindowsStartup.Name = "chkLaunchAtWindowsStartup";
            this.chkLaunchAtWindowsStartup.Size = new System.Drawing.Size(156, 17);
            this.chkLaunchAtWindowsStartup.TabIndex = 6;
            this.chkLaunchAtWindowsStartup.Text = "Launch at Windows startup";
            this.chkLaunchAtWindowsStartup.UseVisualStyleBackColor = true;
            // 
            // grpbxAudioDeviceOptions
            // 
            this.grpbxAudioDeviceOptions.Controls.Add(this.grpbxHotkey);
            this.grpbxAudioDeviceOptions.Controls.Add(this.btnDefaultDevice);
            this.grpbxAudioDeviceOptions.Controls.Add(this.lblStartupDevice);
            this.grpbxAudioDeviceOptions.Controls.Add(this.btnActivateOnStartup);
            this.grpbxAudioDeviceOptions.Controls.Add(this.lstbxAudioDevices);
            this.grpbxAudioDeviceOptions.Location = new System.Drawing.Point(12, 106);
            this.grpbxAudioDeviceOptions.Name = "grpbxAudioDeviceOptions";
            this.grpbxAudioDeviceOptions.Size = new System.Drawing.Size(492, 178);
            this.grpbxAudioDeviceOptions.TabIndex = 7;
            this.grpbxAudioDeviceOptions.TabStop = false;
            this.grpbxAudioDeviceOptions.Text = "Audio Device Options";
            // 
            // grpbxHotkey
            // 
            this.grpbxHotkey.Controls.Add(this.chkEnableHotkey);
            this.grpbxHotkey.Controls.Add(this.btnSetHotkey);
            this.grpbxHotkey.Controls.Add(this.txtHotkey);
            this.grpbxHotkey.Location = new System.Drawing.Point(287, 74);
            this.grpbxHotkey.Name = "grpbxHotkey";
            this.grpbxHotkey.Size = new System.Drawing.Size(199, 76);
            this.grpbxHotkey.TabIndex = 15;
            this.grpbxHotkey.TabStop = false;
            this.grpbxHotkey.Text = "Hotkey";
            // 
            // chkEnableHotkey
            // 
            this.chkEnableHotkey.AutoSize = true;
            this.chkEnableHotkey.Location = new System.Drawing.Point(6, 19);
            this.chkEnableHotkey.Name = "chkEnableHotkey";
            this.chkEnableHotkey.Size = new System.Drawing.Size(96, 17);
            this.chkEnableHotkey.TabIndex = 13;
            this.chkEnableHotkey.Text = "Enable Hotkey";
            this.chkEnableHotkey.UseVisualStyleBackColor = true;
            this.chkEnableHotkey.CheckedChanged += new System.EventHandler(this.chkEnableHotkey_CheckedChanged);
            // 
            // btnSetHotkey
            // 
            this.btnSetHotkey.Enabled = false;
            this.btnSetHotkey.Location = new System.Drawing.Point(118, 40);
            this.btnSetHotkey.Name = "btnSetHotkey";
            this.btnSetHotkey.Size = new System.Drawing.Size(75, 23);
            this.btnSetHotkey.TabIndex = 9;
            this.btnSetHotkey.Text = "SetHotkey";
            this.btnSetHotkey.UseVisualStyleBackColor = true;
            // 
            // txtHotkey
            // 
            this.txtHotkey.Enabled = false;
            this.txtHotkey.Location = new System.Drawing.Point(6, 43);
            this.txtHotkey.Name = "txtHotkey";
            this.txtHotkey.Size = new System.Drawing.Size(106, 20);
            this.txtHotkey.TabIndex = 8;
            this.txtHotkey.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtHotkey_KeyDown);
            // 
            // btnDefaultDevice
            // 
            this.btnDefaultDevice.Enabled = false;
            this.btnDefaultDevice.Location = new System.Drawing.Point(287, 45);
            this.btnDefaultDevice.Name = "btnDefaultDevice";
            this.btnDefaultDevice.Size = new System.Drawing.Size(116, 23);
            this.btnDefaultDevice.TabIndex = 14;
            this.btnDefaultDevice.Text = "Default device";
            this.btnDefaultDevice.UseVisualStyleBackColor = true;
            this.btnDefaultDevice.Click += new System.EventHandler(this.btnDefaultDevice_Click);
            // 
            // lblStartupDevice
            // 
            this.lblStartupDevice.AutoSize = true;
            this.lblStartupDevice.Location = new System.Drawing.Point(6, 153);
            this.lblStartupDevice.Name = "lblStartupDevice";
            this.lblStartupDevice.Size = new System.Drawing.Size(79, 13);
            this.lblStartupDevice.TabIndex = 8;
            this.lblStartupDevice.Text = "Startup device:";
            // 
            // btnActivateOnStartup
            // 
            this.btnActivateOnStartup.Enabled = false;
            this.btnActivateOnStartup.Location = new System.Drawing.Point(287, 16);
            this.btnActivateOnStartup.Name = "btnActivateOnStartup";
            this.btnActivateOnStartup.Size = new System.Drawing.Size(116, 23);
            this.btnActivateOnStartup.TabIndex = 8;
            this.btnActivateOnStartup.Text = "Activate on startup";
            this.btnActivateOnStartup.UseVisualStyleBackColor = true;
            this.btnActivateOnStartup.Click += new System.EventHandler(this.btnActivateOnStartup_Click);
            // 
            // lstbxAudioDevices
            // 
            this.lstbxAudioDevices.FormattingEnabled = true;
            this.lstbxAudioDevices.Location = new System.Drawing.Point(3, 16);
            this.lstbxAudioDevices.Name = "lstbxAudioDevices";
            this.lstbxAudioDevices.Size = new System.Drawing.Size(278, 134);
            this.lstbxAudioDevices.TabIndex = 0;
            this.lstbxAudioDevices.SelectedIndexChanged += new System.EventHandler(this.lstbxAudioDevices_SelectedIndexChanged);
            // 
            // btnAbout
            // 
            this.btnAbout.Location = new System.Drawing.Point(267, 290);
            this.btnAbout.Name = "btnAbout";
            this.btnAbout.Size = new System.Drawing.Size(75, 23);
            this.btnAbout.TabIndex = 8;
            this.btnAbout.Text = "About";
            this.btnAbout.UseVisualStyleBackColor = true;
            this.btnAbout.Click += new System.EventHandler(this.btnAbout_Click);
            // 
            // ADQCPreferencesForm
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(516, 325);
            this.Controls.Add(this.btnAbout);
            this.Controls.Add(this.grpbxAudioDeviceOptions);
            this.Controls.Add(this.chkLaunchAtWindowsStartup);
            this.Controls.Add(this.chkShowNotifyIconOnStart);
            this.Controls.Add(this.grpbxShownDevices);
            this.Controls.Add(this.btnDefaults);
            this.Controls.Add(this.chkShowAudioDevicesAsMenuItem);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnCancel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "ADQCPreferencesForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ADQC Preferences";
            this.Load += new System.EventHandler(this.ADQCPreferencesForm_Load);
            this.grpbxShownDevices.ResumeLayout(false);
            this.grpbxShownDevices.PerformLayout();
            this.grpbxAudioDeviceOptions.ResumeLayout(false);
            this.grpbxAudioDeviceOptions.PerformLayout();
            this.grpbxHotkey.ResumeLayout(false);
            this.grpbxHotkey.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnDefaults;
        private System.Windows.Forms.CheckBox chkShowAudioDevicesAsMenuItem;
        private System.Windows.Forms.GroupBox grpbxShownDevices;
        private System.Windows.Forms.CheckBox chkShowActive;
        private System.Windows.Forms.CheckBox chkShowDisabled;
        private System.Windows.Forms.CheckBox chkShowUnplugged;
        private System.Windows.Forms.CheckBox chkShowNotifyIconOnStart;
        private System.Windows.Forms.CheckBox chkLaunchAtWindowsStartup;
        private System.Windows.Forms.GroupBox grpbxAudioDeviceOptions;
        private System.Windows.Forms.ListBox lstbxAudioDevices;
        private System.Windows.Forms.CheckBox chkEnableHotkey;
        private System.Windows.Forms.Label lblStartupDevice;
        private System.Windows.Forms.Button btnActivateOnStartup;
        private System.Windows.Forms.Button btnDefaultDevice;
        private System.Windows.Forms.TextBox txtHotkey;
        private System.Windows.Forms.GroupBox grpbxHotkey;
        private System.Windows.Forms.Button btnSetHotkey;
        private System.Windows.Forms.Button btnAbout;
    }
}

