﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.IO;

namespace AudioDeviceQuickChange
{
    public static class DataManager
    {
        public static string DataFolder = @".\Data\";
        public static string PreferencesFile = DataFolder + "prefs.xml";

        private static XDocument xPrefs;

        private static void VerifyDataFolder()
        {
            if (!Directory.Exists(DataFolder))
                Directory.CreateDirectory(DataFolder);

            if (!File.Exists(PreferencesFile))
                xPrefs = new XDocument(new XDeclaration("1.0", "UTF-8", "yes"),
                                        new XElement("AudioDevices"));
        }

        //public static void SaveDeviceList(List<AudioDevicePlus> deviceList)
        //{
        //    VerifyDataFolder();

        //    xPrefs.Element("AudioDevices").RemoveAll();
            
        //    foreach (AudioDevicePlus adp in deviceList)
        //    {
        //        xPrefs.Element("AudioDevices").Add(new XElement("ActivateOnStartUp", adp.ActivateOnStartup),
        //                                           new XElement("HotkeyEnabled", adp.HotkeyEnabled),
        //                                           new XElement("HotkeyModifiers"));

        //        if (adp.HotkeyModifiers.Length > 0)
        //        {
        //            int cnt = 0;
        //            foreach (char hkChar in adp.HotkeyModifiers)
        //            {
        //                if (hkChar != null)
        //                {
        //                    cnt++;
        //                    xPrefs.Element("AudioDevices").Element("HotkeyModifiers").Add(new XElement("Modifier" + cnt.ToString(), adp.HotkeyModifiers[cnt]));
        //                }
        //            }
        //        }

        //        xPrefs.Add(new XElement("Hotkey", adp.Hotkey));
        //    }

        //    xPrefs.Save(PreferencesFile);
        //}
    }
}
