﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

using AudioDeviceUtil;
using AudioDeviceQuickChange.Properties;

//
using System.Diagnostics;
//

namespace AudioDeviceQuickChange
{
    static class Program
    {
        static NotifyIcon nIcon;
        static ContextMenu cMenu;
        /*MenuItems*/
        static MenuItem mnuExit;
        static MenuItem mnuPreferences;

        public static AudioDeviceManager ADM;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.ApplicationExit += new EventHandler(Application_Exiting);

            ADM = new AudioDeviceManager();

            //
            //ADM = new AudioDeviceManager(true);
            //ADM.AudioDeviceEvent += new EventHandler<AudioDeviceNotificationEventArgs>(AudioDevice_Event);
            ////ADM.AudioDeviceEvent += AudioDevice_Event;
            //

            nIcon = new NotifyIcon();
            cMenu = new ContextMenu();
            mnuPreferences = new MenuItem("Preferences");
            mnuExit = new MenuItem("Exit");
            
            /*Setup MenuItems*/            
            mnuPreferences.Name = "mnuPreferences";
            mnuPreferences.Click += new EventHandler(mnuPreferences_Clicked);
            mnuExit.Name = "mnuExit";
            mnuExit.Click += new EventHandler(mnuExit_Clicked);

            /*Initialize ContextMenu cMenu*/
            cMenu.Name = "cMenu";
            cMenu.Popup += new EventHandler(cMenu_Popup);
            FillContextMenu();
            //FillContextMenu(true);    //<?>//

            /*Setup the NotifyIcon*/
            nIcon.Text = "Audio Device Quick Change";
            nIcon.Icon = AudioDeviceQuickChange.Properties.Resources.adqc16x;
            nIcon.Visible = true;
            nIcon.ContextMenu = cMenu;
            nIcon.BalloonTipIcon = ToolTipIcon.Info;
            nIcon.BalloonTipTitle = "Audio Device Quick Change";
            nIcon.BalloonTipText = "Audio Device Quick Change application is running!";

            if (Settings.Default.ShowNotifyIconOnStart)
                nIcon.ShowBalloonTip(1000);            

            Application.Run();

            //if (Settings.Default.StartupAudioDevice != "SystemDefault")   //<?>//
            //    ActivateAudioDevice(Settings.Default.StartupAudioDevice);
        }

        #region Methods
        private static void ActivateAudioDevice(string deviceName)
        {
            ADM.DefaultPlaybackDeviceName = deviceName;
        }

        private static void AddAudioDeviceToMenu(AudioDevice audioDevice, MenuItem mnuAD)
        {
            MenuItem mnuDeviceItem = new MenuItem(audioDevice.FriendlyName);
            mnuDeviceItem.Click += new EventHandler(mnuDeviceItem_Clicked);

            if (audioDevice.IsDefaultDevice)
                mnuDeviceItem.Checked = true;

            if (mnuAD == null)
                cMenu.MenuItems.Add(mnuDeviceItem);
            else
                mnuAD.MenuItems.Add(mnuDeviceItem);
        }

        //<!>//
        public static void FillContextMenu()
        {
            MenuItem mnuAudioDevices = null;
            List<AudioDevice> templist = null;

            cMenu.MenuItems.Clear();

            //
            try
            {
                ADM.UpdatePlaybackDeviceList();
                templist = ADM.PlaybackDevices.OrderBy(a => a.FriendlyName).ToList();
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"Exception: {ex.Message} [FillContextMenu()]");
            }
            //

            /*Setup Audio Device MenuItems*/
            if (Settings.Default.ShowAudioDevicesAsMenuItem)
            {
                mnuAudioDevices = new MenuItem("Audio Devices");
                mnuAudioDevices.Name = "mnuAudioDevices";
                cMenu.MenuItems.Add(mnuAudioDevices);

                foreach (AudioDevice ad in templist)
                {
                    switch (ad.DeviceState)
                    {
                        case AudioDeviceStateType.Active:
                            if (Settings.Default.ShowActiveAudioDevices)
                                AddAudioDeviceToMenu(ad, mnuAudioDevices);
                            break;
                        case AudioDeviceStateType.Disabled:
                            if (Settings.Default.ShowDisabledAudioDevices)
                                AddAudioDeviceToMenu(ad, mnuAudioDevices);
                            break;
                        case AudioDeviceStateType.Unplugged:
                            if (Settings.Default.ShowUnpluggedAudioDevices)
                                AddAudioDeviceToMenu(ad, mnuAudioDevices);
                            break;
                    }
                }
            }
            else
            {
                foreach (AudioDevice ad in templist)
                {
                    switch (ad.DeviceState)
                    {
                        case AudioDeviceStateType.Active:
                            if (Settings.Default.ShowActiveAudioDevices)
                                AddAudioDeviceToMenu(ad, null);
                            break;
                        case AudioDeviceStateType.Disabled:
                            if (Settings.Default.ShowDisabledAudioDevices)
                                AddAudioDeviceToMenu(ad, null);
                            break;
                        case AudioDeviceStateType.Unplugged:
                            if (Settings.Default.ShowUnpluggedAudioDevices)
                                AddAudioDeviceToMenu(ad, null);
                            break;
                    }
                }
            }
            
            /*Setup ContextMenu*/
            cMenu.MenuItems.Add("-");
            cMenu.MenuItems.Add(mnuPreferences);
            cMenu.MenuItems.Add("-");
            cMenu.MenuItems.Add(mnuExit);      
        }

        public static List<AudioDevice> GetAudioDeviceList()
        {
            List<AudioDevice> DeviceList = new List<AudioDevice>();

            foreach (AudioDevice ad in ADM.PlaybackDevices)
            {
                switch (ad.DeviceState)
                {
                    case AudioDeviceStateType.Active:
                        if (Settings.Default.ShowActiveAudioDevices)
                            DeviceList.Add(ad);
                        break;
                    case AudioDeviceStateType.Disabled:
                        if (Settings.Default.ShowDisabledAudioDevices)
                            DeviceList.Add(ad);
                        break;
                    case AudioDeviceStateType.Unplugged:
                        if (Settings.Default.ShowUnpluggedAudioDevices)
                            DeviceList.Add(ad);
                        break;
                }
            }
            return DeviceList;            
        }

        ////<!>//
        //public static List<AudioDevicePlus> GetAudioDevicePlusList()
        //{
        //    List<AudioDevicePlus> DevicePlusList = new List<AudioDevicePlus>();
            
        //    foreach (AudioDevice ad in ADM.PlaybackDevices)
        //    {
        //        switch (ad.DeviceState)
        //        {
        //            case AudioDeviceStateType.Active:
        //                if (Settings.Default.ShowActiveAudioDevices)
        //                    DevicePlusList.Add(new AudioDevicePlus(ad));
        //                break;
        //            case AudioDeviceStateType.Disabled:
        //                if (Settings.Default.ShowDisabledAudioDevices)
        //                    DevicePlusList.Add(new AudioDevicePlus(ad));
        //                break;
        //            case AudioDeviceStateType.Unplugged:
        //                if (Settings.Default.ShowUnpluggedAudioDevices)
        //                    DevicePlusList.Add(new AudioDevicePlus(ad));
        //                break;
        //        }
        //    }
        //    return DevicePlusList;
        //}
        #endregion

        #region Event Handlers
        private static void Application_Exiting(object sender, EventArgs e)
        {
            if (nIcon != null)
                nIcon.Dispose();
        }

        //
        public static void AudioDevice_Event(object sender, AudioDeviceNotificationEventArgs e)
        {

            //AudioDeviceNotification audioNotify = (AudioDeviceNotification)sender;
            //
            //AudioDeviceManager adm = new AudioDeviceManager();
            //

            switch (e.Reason)
            {
                case AudioDeviceNotificationEventType.DefaultDeviceChanged:
                    Debug.WriteLine("Device Changed:");
                    Debug.WriteLine($"DeviceID: {e.DeviceId}");
                    break;
                case AudioDeviceNotificationEventType.DeviceAdded:
                    Debug.WriteLine("Device Added:");
                    break;
                case AudioDeviceNotificationEventType.DeviceRemoved:
                    Debug.WriteLine("Device Removed:");
                    break;
                case AudioDeviceNotificationEventType.DeviceStateChanged:
                case AudioDeviceNotificationEventType.PropertyValueChanged:
                    //?
                    break;
            }
        }
        //

        private static void mnuDeviceItem_Clicked(object sender, EventArgs e)
        {
            MenuItem mi = (MenuItem)sender;
            AudioDevice ad = ADM.PlaybackDevices.Single(a => a.FriendlyName == mi.Text);
            ADM.DefaultPlaybackDeviceName = ad.FriendlyName;

            FillContextMenu();
        }

        private static void mnuExit_Clicked(object sender, EventArgs e)
        {
            if (nIcon != null)
                nIcon.Dispose();

            Application.ExitThread();
        }

        private static void mnuPreferences_Clicked(object sender, EventArgs e)
        {
            ADQCPreferencesForm prefs = new ADQCPreferencesForm();
            prefs.ShowDialog();
        }

        private static void cMenu_Popup(object sender, EventArgs e)
        {
            ADM.UpdatePlaybackDeviceList();
            FillContextMenu();
        }
        #endregion
    }
}
