﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AudioDeviceUtil;

namespace AudioDeviceQuickChange
{
    class AudioDevicePlus
    {
        private AudioDevice _AudioDeviceObject;
        private bool _ActivateOnStartup;
        private bool _HotkeyEnabled;
        private Hotkey _DeviceHotkey;

        #region Properties
        public AudioDevice AudioDeviceObject
        {
            get { return _AudioDeviceObject; }
        }

        public bool ActivateOnStartup
        {
            get { return _ActivateOnStartup; }
            set { _ActivateOnStartup = value; }
        }

        public bool HotkeyEnabled
        {
            get { return _HotkeyEnabled; }
            set { _HotkeyEnabled = value; }
        }

        public Hotkey DeviceHotkey
        {
            get { return _DeviceHotkey;  }
            set { _DeviceHotkey = value; }
        }
        #endregion

        #region Constructors
        public AudioDevicePlus(AudioDevice device)
        {
            _AudioDeviceObject = device;
            _ActivateOnStartup = false;
            _HotkeyEnabled = false;
            _DeviceHotkey = null;
        }
        #endregion
    }
}
