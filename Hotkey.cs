﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Windows.Forms;



namespace AudioDeviceQuickChange
{
    public class Hotkey
    {
        #region Interop Prototypes

        [DllImport("user32.dll")]
        private static extern bool RegisterHotkey(IntPtr hWnd, int id, int fsModifiers, int vk);

        [DllImport("user32.dll")]
        private static extern bool UnregisterHotkey(IntPtr hWnd, int id);

        #endregion

        #region Constants

        const int NONE = 0x0000;
        const int ALT = 0x0001;
        const int CONTROL = 0x0002;
        const int SHIFT = 0x0003;
        const int WINKEY = 0x0004;
        const int MSGID = 0x0312;

        //const int NONE = 0;
        //const int ALT = 1;
        //const int CONTROL = 2;
        //const int SHIFT = 3;
        //const int WINKEY = 4;
        //const int MSGID = 786;

        #endregion

        #region Fields

        private int                 _modifier;
        private int                 _key;
        private IntPtr              _hWnd;
        private int                 _id;

        #endregion

        #region Constructors

        public Hotkey(int modifier, Keys key, Form form)
        {
            this._modifier = modifier;
            this._key = (int)key;
            this._hWnd = form.Handle;
            this._id = this.GetHashCode();
        }

        #endregion

        #region Methods

        public override int GetHashCode()
        {
            return (_modifier ^ _key ^ _hWnd.ToInt32());
        }

        public bool Register()
        {
            return (RegisterHotkey(this._hWnd, this._id, this._modifier, this._key));
        }

        public bool Unregister()
        {
            return (UnregisterHotkey(this._hWnd, this._id));
        }

        #endregion
    }
}
