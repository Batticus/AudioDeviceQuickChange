﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AudioDeviceUtil;

namespace AudioDeviceQuickChange
{
    public class AudioDeviceEX
    {
        private AudioDevice _Device;
        private bool _ActivateOnStartup;
        private bool _HotkeyEnabled;
        //private System.Windows.Forms.Keys _Hotkeys;??

        #region Properties
        public AudioDevice Device
        {
            get { return _Device; }
        }

        public bool ActivateOnStartup
        {
            get { return _ActivateOnStartup; }
            set { _ActivateOnStartup = value; }
        }

        public bool HotkeyEnabled
        {
            get { return _HotkeyEnabled; }
            set { _HotkeyEnabled = value; }
        }
        #endregion
    }
}
