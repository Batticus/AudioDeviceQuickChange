﻿using System;
using System.Collections.Generic;
//using System.ComponentModel;
using System.Data;
//using System.Drawing;
using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

using AudioDeviceUtil;
using AudioDeviceQuickChange.Properties;

namespace AudioDeviceQuickChange
{
    public partial class ADQCPreferencesForm : Form
    {
        private /*readonly*/ string _StartupPath = Environment.GetFolderPath(Environment.SpecialFolder.Startup);

        private string startupDevice;
        //private List<>

        #region Constructors
        public ADQCPreferencesForm()
        {
            InitializeComponent();
        }
        #endregion

        #region Methods
        private void CreateStartupShortcut()
        {
            IWshRuntimeLibrary.WshShell wshShell = new IWshRuntimeLibrary.WshShell();
            IWshRuntimeLibrary.IWshShortcut iwshShortcut = wshShell.CreateShortcut(_StartupPath + "\\ADQC.lnk");

            iwshShortcut.TargetPath = Application.ExecutablePath;   //System.Reflection.Assembly.GetEntryAssembly().Location;
            iwshShortcut.WorkingDirectory = Application.StartupPath;

            iwshShortcut.Save();
        }

        private bool IsValidHotkey(Keys key)
        {
            switch (key)
            {
                case Keys.Alt:
                case Keys.Apps:
                case Keys.Back:
                case Keys.ControlKey:
                case Keys.Capital:
                case Keys.Escape:
                case Keys.Menu:
                case Keys.NumLock:
                case Keys.Pause:
                case Keys.PrintScreen:
                case Keys.Return:
                case Keys.Scroll:
                case Keys.ShiftKey:
                case Keys.Space:
                case Keys.Tab:
                    return (false);
            }
            return (true);
        }

        private void FillListBox()
        {
            List<AudioDevice> adl = Program.GetAudioDeviceList().OrderBy(d => d.FriendlyName).ToList();

            lstbxAudioDevices.Items.Clear();

            foreach (AudioDevice ad in adl)
            {
                lstbxAudioDevices.Items.Add(ad.FriendlyName);
            }
        }
        #endregion

        #region CustomEventHandlers
        private void ShownDevices_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chk = (CheckBox)sender;

            switch (chk.Name)
            {
                case "chkShowActive":
                    Settings.Default.ShowActiveAudioDevices = chkShowActive.Checked;
                    break;
                case "chkShowDisabled":
                    Settings.Default.ShowDisabledAudioDevices = chkShowDisabled.Checked;
                    break;
                case "chkShowUnplugged":
                    Settings.Default.ShowUnpluggedAudioDevices = chkShowUnplugged.Checked;
                    break;
            }

            //??
            Settings.Default.Save();
            FillListBox();
        }
        #endregion

        /*Event Handlers*/
        private void ADQCPreferencesForm_Load(object sender, EventArgs e)
        {
            chkShowAudioDevicesAsMenuItem.Checked = Settings.Default.ShowAudioDevicesAsMenuItem;
            chkShowActive.Checked = Settings.Default.ShowActiveAudioDevices;
            chkShowDisabled.Checked = Settings.Default.ShowDisabledAudioDevices;
            chkShowUnplugged.Checked = Settings.Default.ShowUnpluggedAudioDevices;
            chkShowNotifyIconOnStart.Checked = Settings.Default.ShowNotifyIconOnStart;
            chkLaunchAtWindowsStartup.Checked = Settings.Default.LaunchAtWindowsStartup;

            startupDevice = Settings.Default.StartupAudioDevice;
            lblStartupDevice.Text = "Startup device: " + startupDevice;

            FillListBox();
        }

        private void btnDefaults_Click(object sender, EventArgs e)
        {
            chkShowAudioDevicesAsMenuItem.Checked = true;
            chkShowActive.Checked = true;
            chkShowDisabled.Checked = false;
            chkShowUnplugged.Checked = false;
            chkShowNotifyIconOnStart.Checked = true;
            chkLaunchAtWindowsStartup.Checked = false;
            startupDevice = "SystemDefault";
            lblStartupDevice.Text = "Startup device: " + startupDevice;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            //if (chkLaunchAtWindowsStartup.Checked != Settings.Default.LaunchAtWindowsStartup)
            //    if (chkLaunchAtWindowsStartup.Checked)
            //        if (!File.Exists(_StartupPath + "\\ADQC.lnk"))
            //            CreateStartupShortcut();
            //        else if (File.Exists(_StartupPath + "\\ADQC.lnk"))
            //            File.Delete(_StartupPath + "\\ADQC.lnk");

            //^

            if (chkLaunchAtWindowsStartup.Checked)
            {
                if (!File.Exists($"{_StartupPath}\\ADQC.lnk"))
                    CreateStartupShortcut();
            }
            else
            {
                if (File.Exists($"{_StartupPath}\\ADQC.lnk"))
                    File.Delete($"{_StartupPath}\\ADQC.lnk");
            }

            Settings.Default.ShowNotifyIconOnStart = chkShowNotifyIconOnStart.Checked;
            Settings.Default.LaunchAtWindowsStartup = chkLaunchAtWindowsStartup.Checked;
            Settings.Default.StartupAudioDevice = startupDevice;

            if (chkShowAudioDevicesAsMenuItem.Checked != Settings.Default.ShowAudioDevicesAsMenuItem)
            {
                if (MessageBox.Show("One or more options require ADQC to restart.\r\n\r\nContinue?",
                                    "Restart ADQC",
                                    MessageBoxButtons.YesNo,
                                    MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    Settings.Default.ShowAudioDevicesAsMenuItem = chkShowAudioDevicesAsMenuItem.Checked;
                    Settings.Default.Save();
                    Application.Restart();
                }
            }

            //THIS CODE IS NOT REACHED IF THE APPLICATION IS RESTARTED. ^

            Settings.Default.Save();

            /*Perform tasks based on setting changes*/
            Program.FillContextMenu();
        }

        private void chkEnableHotkey_CheckedChanged(object sender, EventArgs e)
        {
            txtHotkey.Enabled = chkEnableHotkey.Checked;
            btnSetHotkey.Enabled = chkEnableHotkey.Checked;
        }

        private void lstbxAudioDevices_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lstbxAudioDevices.SelectedItems.Count <= 0)
            {
                btnActivateOnStartup.Enabled = false;
                btnDefaultDevice.Enabled = false;
            }
            else
            {
                btnActivateOnStartup.Enabled = true;
                btnDefaultDevice.Enabled = true;
            }
        }

        private void btnActivateOnStartup_Click(object sender, EventArgs e)
        {
            startupDevice = lstbxAudioDevices.SelectedItems[0].ToString();
            lblStartupDevice.Text = "Startup device: " + startupDevice;
        }

        private void btnDefaultDevice_Click(object sender, EventArgs e)
        {
            startupDevice = "SystemDefault";
            lblStartupDevice.Text = "Startup device: " + startupDevice;
        }

        private void txtHotkey_KeyDown(object sender, KeyEventArgs e)
        {
            string hotkeyString = string.Empty;

            e.SuppressKeyPress = true;

            if (e.Alt || e.Control || e.Shift)
            {
                hotkeyString = e.Modifiers.ToString();
                if (IsValidHotkey(e.KeyCode))
                    hotkeyString += $" + {e.KeyCode.ToString()}";
            }
            else
            {
                hotkeyString = e.KeyCode.ToString();
            }

            if (hotkeyString.Contains(" + "))
            {
                txtHotkey.Text = hotkeyString;
                //To hotkey register work?
            }
        }

        private void btnAbout_Click(object sender, EventArgs e)
        {
            using (AboutADQC about = new AboutADQC())
            {
                about.ShowDialog();
            }
        }
    }
}
